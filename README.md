Android project that demonstrates 4-directions swipe navigation as into my [Urban Curiosity](https://play.google.com/store/apps/details?id=com.visionapps.urban "Urban Curiosity") applicaton. 

Quick demo of the 4-directions swipe navigation:
[https://vimeo.com/66999298](https://vimeo.com/66999298)

Read more about the implementation here:
[http://vision-apps.blogspot.com/2013/05/4-directions-swipe-navigation.html](http://vision-apps.blogspot.com/2013/05/4-directions-swipe-navigation.html)

